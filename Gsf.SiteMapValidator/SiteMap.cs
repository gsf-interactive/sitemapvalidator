using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Gsf.SiteMapValidator
{
    [XmlRoot(ElementName="urlset")]
    public class SiteMap {
        [XmlElement(ElementName="url")]
        public List<Url> Url { get; set; }
        [XmlAttribute(AttributeName="xmlns")]
        public string Xmlns { get; set; }

        public SiteMap()
        {
            Url = new List<Url>();
        }

        public static SiteMap Deserialize(Stream xmlStream)
        {
            var reader = new XmlTextReader(xmlStream)
            {
                Namespaces = false
            };
            var xmlSerializer = new XmlSerializer(typeof(SiteMap));
            var siteMap = xmlSerializer.Deserialize(reader) as SiteMap;

            return siteMap;
        }
    }
    
    [XmlRoot(ElementName="url")]
    public class Url {
        [XmlElement(ElementName="loc")]
        public string Loc { get; set; }
        [XmlElement(ElementName="lastmod")]
        public DateTime? Lastmod { get; set; }
        [XmlElement(ElementName="changefreq")]
        public UrlChangeFrequency? Changefreq { get; set; }
        [XmlElement(ElementName="priority")]
        public float? Priority { get; set; }
    
        public enum UrlChangeFrequency
        {
            always,
            hourly,
            daily,
            weekly,
            monthly,
            yearly,
            never
        }
    }
}