using System.Net.Http;
using System.Threading.Tasks;

namespace Gsf.SiteMapValidator
{
    public class SiteMapFetcher
    {
        private HttpClient _httpClient;

        public SiteMapFetcher()
        {
            _httpClient = new HttpClient();
        }

        public async Task<SiteMap> FetchAsync(string url)
        {
            var response = await _httpClient.GetAsync($"{url}/sitemap.xml");
            var contentStream = await response.Content.ReadAsStreamAsync();

            var siteMap = SiteMap.Deserialize(contentStream);

            return siteMap;
        }
    }
}