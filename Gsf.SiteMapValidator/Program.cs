﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Kurukuru;

namespace Gsf.SiteMapValidator
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Count() < 1)
            {
                Console.WriteLine("url argument required");
                return 1;
            }
            
            var start = DateTime.Now;
            var url = args[0];
            
            List<string> urls = null;
            Spinner.Start($"Fetching site map from {url}", () =>
            {

                var fetcher = new SiteMapFetcher();
                var siteMap = fetcher.FetchAsync(url).Result;

                urls = siteMap.Url
                    .Select(u => u.Loc).ToList();
            });
        

            var results = new List<UrlValidationResult>();

            var done = 0;
            var total = urls?.Count ?? 0;

            Spinner.Start($"Validating URLs {done} of {total}", (spinner) => {
                if (urls is null)
                {
                    return;
                }
                
                var urlValidator = new UrlValidator(new HttpClient());
                Parallel.ForEach(urls, u =>
                {
                    var result = ProcessUrl(urlValidator, u);
                    lock (results)
                    {
                        results.Add(result);
                    }

                    Interlocked.Increment(ref done);
                    spinner.Text = $"Validating URLs {done} of {total}";
                });
            });

            Console.WriteLine("Problems found...");
            results.Where(r => !r.IsValid).ToList().ForEach(r => Console.WriteLine($"{r.Url} - {r.Message}"));
            Console.WriteLine($"Validation took {(DateTime.Now - start).Seconds} seconds");

            return 0;
        }

        private static UrlValidationResult ProcessUrl(UrlValidator urlValidator, string url)
        {
            try
            {
                return urlValidator.ValidateAsync(url).Result;
            }
            catch (Exception e)
            {
                return new UrlValidationResult
                {
                    IsValid = false,
                    Message = $"Exception thrown: {e.Message}",
                    Url = url
                };
            }
        }
    }
}