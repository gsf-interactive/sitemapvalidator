using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Gsf.SiteMapValidator
{
    public class UrlValidator
    {
        private HttpClient _httpClient;

        public UrlValidator(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        
        public async Task<UrlValidationResult> ValidateAsync(string url)
        {
            var response = await _httpClient.GetAsync(url);

            var statusCodeRange = (int)response.StatusCode / 100;
            
            return statusCodeRange switch
            {
                2 => new UrlValidationResult { Url = url, IsValid = true, Message = "OK"},
                3 => new UrlValidationResult { Url = url, IsValid = false, Message = $"Redirects to {response.Headers.Location}"},
                _ => new UrlValidationResult { Url = url, IsValid = false, Message = $"Error response - {response.StatusCode}"}
            };
        }
    }

    public class UrlValidationResult
    {
        public string Url { get; set; }
        public bool IsValid { get; set; }
        public string Message { get; set; }
    }
    
}